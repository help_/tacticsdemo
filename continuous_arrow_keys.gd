extends Node

const ui_actions := [
	"ui_left",
	"ui_up",
	"ui_right",
	"ui_down"
]
const ui_directions := [
	Vector2i.LEFT,
	Vector2i.UP,
	Vector2i.RIGHT,
	Vector2i.DOWN
]

var direction := Vector2i.ZERO

signal sig_direction_changed(direction: Vector2i)

func _process(_delta: float) -> void:
	var direction_prev := direction
	var direction_instant = Vector2i.ZERO
	for i in range(ui_actions.size()):
		if Input.is_action_just_pressed(ui_actions[i]):
			direction_instant = ui_directions[i]
			break
	if direction_instant != Vector2i.ZERO:
		direction = direction_instant
	else:
		var direction_continuous = Vector2i.ZERO
		for i in range(ui_actions.size()):
			if direction == ui_directions[i] and Input.is_action_pressed(ui_actions[i]):
				direction_continuous = ui_directions[i]
				break
		if direction_continuous != Vector2i.ZERO:
			direction = direction_continuous
		else:
			var direction_remaining = Vector2i.ZERO
			for i in range(ui_actions.size()):
				if Input.is_action_pressed(ui_actions[i]):
					direction_remaining = ui_directions[i]
					break
			if direction_remaining != Vector2i.ZERO:
				direction = direction_remaining
			else:
				direction = Vector2i.ZERO
	if direction != direction_prev:
		sig_direction_changed.emit(direction)
