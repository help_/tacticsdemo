class_name State
extends Node

signal state_finished
signal state_started

func _enter_state() -> void:
	pass

func _exit_state() -> void:
	pass
