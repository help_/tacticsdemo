extends Object
class_name Utils

static func coordinates_for(coordinate: Vector2i, distance: int):
	var directions = [
		Vector2i.UP,
		Vector2i.RIGHT,
		Vector2i.DOWN,
		Vector2i.LEFT
	]
	var coordinates = []
	for index in range(directions.size()):
		var direction = directions[index]
		var next_direction = directions[(index + 1) % directions.size()]
		for i in range(distance):
			for j in range(1, distance + 1 - i):
				var coord = coordinate + direction * j + next_direction * i
				coordinates.push_back(coord)
	return coordinates

static func coordinates_for_movement(start_coord: Vector2i, distance: int, valid_coord_func: Callable) -> Array[Vector2i]:
	var directions = [
		Vector2i.UP,
		Vector2i.RIGHT,
		Vector2i.DOWN,
		Vector2i.LEFT
	]
	var coordinates: Array[Vector2i] = [start_coord]
	var search_index = 0
	
	for step in distance:
		var temp_search_index = len(coordinates)
		for index in range(search_index, len(coordinates)):
			var coord = coordinates[index]
			for direction in directions:
				var new_coord = coord + direction
				if not coordinates.has(new_coord):
					if valid_coord_func.call(new_coord):
						coordinates.push_back(new_coord)
		search_index = temp_search_index
	coordinates.pop_front()
	return coordinates	
