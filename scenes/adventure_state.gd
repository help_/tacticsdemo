class_name AdventureState
extends State

func _enter_state() -> void:
	state_started.emit()
	

func _exit_state() -> void:
	#
	state_finished.emit()
