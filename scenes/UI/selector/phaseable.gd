extends Node
class_name Phaseable

enum Phase {MOVE, ATTACK, DONE}

@export var phase: Phase = Phase.DONE

signal sig_phase_changed(phase: Phase)

func next_phase():
	match phase:
		Phase.MOVE:
			phase = Phase.ATTACK
			sig_phase_changed.emit(phase)
		Phase.ATTACK:
			phase = Phase.DONE
			sig_phase_changed.emit(phase)
		Phase.DONE:
			phase = Phase.MOVE
			sig_phase_changed.emit(phase)

func set_phase(new_phase: Phase):
	phase = new_phase
	sig_phase_changed.emit(phase)

func reset_phase():
	phase = Phase.DONE
	sig_phase_changed.emit(phase)

func is_move() -> bool:
	return phase == Phase.MOVE
func is_attack() -> bool:
	return phase == Phase.ATTACK
func is_done() -> bool:
	return phase == Phase.DONE
