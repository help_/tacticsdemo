class_name Selector
extends Node2D

@export var movetile = preload("res://scenes/UI/color_tile/tile_move.tscn")
@export var attacktile = preload("res://scenes/UI/color_tile/tile_attack.tscn")
@export var tile: TileMap
@export var team_manager: TeamManager
@export var turn_manager: TurnManager

@onready var phaseable := $Phaseable as Phaseable

var character: Node2D
var state_move:int = 0
var n = Node2D.new()
var astar_grid = AStarGrid2D.new()
var grid_size
var cell_size = Vector2i(1, 1)
var is_moving = false
var turn_start_pos


@export var tile_move_time = 0.1

func _ready():
	tile.add_child(n)
	initialize_grid()
	add_char_pos_to_grid()
	turn_manager.sig_turn_changed.connect(_on_turn_changed)
	phaseable.sig_phase_changed.connect(_on_phase_changed)


func _on_turn_changed(from: Team, to: Team):
	for member in to.members:
		member.get_parent().get_node('TurnAction').set_action(true)
	phaseable.reset_phase()
	
	
func _on_phase_changed(phase: Phaseable.Phase):
	match(phaseable.phase):
		Phaseable.Phase.MOVE:
			hide_tiles()
			display_move()
		Phaseable.Phase.ATTACK:
			display_attack()
		Phaseable.Phase.DONE:
			hide_tiles()

func add_char_pos_to_grid():
	var teams = team_manager.get_teams() as Array[Team]
	for team: Team in teams:
		for char in team.members:
			var space = tile.local_to_map(char.get_parent().position)
			print(space)
			astar_grid.set_point_solid(map_to_grid(space), true) 
			
			
func update_grid():
	astar_grid.set_point_solid( map_to_grid( tile.local_to_map(turn_start_pos)), false) 
	add_char_pos_to_grid()

func initialize_grid():
	grid_size = Vector2i(get_viewport_rect().size) / cell_size
	astar_grid.size = grid_size
	astar_grid.cell_size = cell_size
	astar_grid.diagonal_mode = AStarGrid2D.DIAGONAL_MODE_NEVER
	astar_grid.offset = cell_size / 2
	var impass = tile.get_used_cells(0)
	astar_grid.update()
	print(impass)
	for cell in impass:
		astar_grid.set_point_solid( map_to_grid(cell), true) 
	
func map_to_grid(val):
	var new_val = Vector2i(val.x+12, val.y+7)
	return new_val
	
func grid_to_map(val):
	var new_val = Vector2i(val.x-12, val.y-7)
	return new_val

func select_on_unit():
	var teams = team_manager.get_teams() as Array[Team]
	for team: Team in teams:
		if team == turn_manager.current_team_turn():
			for member in team.members:
				var mem = member.get_parent() as Node2D
				if tile.local_to_map(position) == tile.local_to_map(mem.position):
					if mem.get_node('TurnAction').get_action():
						character = mem
						return true
	return false

func _process(delta):
	if Input.is_action_just_pressed("cancel_button"):
		print('start', phaseable.phase)
		match phaseable.phase:
			Phaseable.Phase.DONE:
				pass
			Phaseable.Phase.MOVE: 
				phaseable.set_phase(Phaseable.Phase.DONE)
			Phaseable.Phase.ATTACK:
				character.position = turn_start_pos
				phaseable.set_phase(Phaseable.Phase.MOVE)
		print('end', state_move)
		
	if Input.is_action_just_pressed("accept_button"):
		print('start', phaseable.phase)
		match phaseable.phase:
			Phaseable.Phase.DONE:
				if select_on_unit():
					phaseable.next_phase()
			Phaseable.Phase.MOVE:
				move_character()
			Phaseable.Phase.ATTACK: 
				if attack_mode():
					update_grid()
					phaseable.set_phase(Phaseable.Phase.DONE)
		print('end', phaseable.phase)
		
func deal_damage():
	print('damage')
	var enemy = get_selected_enemy()
	var health_data := enemy.get_node('healthData') as HealthData
	var callable := enemy_died.bind(enemy)
	health_data.sig_died.connect(callable)
	health_data.damage_health(character.get_node("attackData").get_attack_damage())
	health_data.sig_died.disconnect(callable)
	print(enemy.get_node('healthData').get_current_health())

func enemy_died(enemy: Node2D):
	var teamable := enemy.get_node("Teamable") as Teamable
	team_manager._remove_team_member(teamable.team, teamable)
	
	
func get_selected_enemy():
	var enemies = get_enemies()
	for enemy in enemies:
		var ene = enemy.get_parent()
		if tile.local_to_map(position) == tile.local_to_map(ene.position):
			return ene
	return null
	
func get_enemies():
	var teams = team_manager.get_teams() as Array[Team]
	for team: Team in teams:
		if not team.number == character.get_node("Teamable").team.number:
			return team.members
			
	return null
			
func check_if_valid_target()->bool:
	var enemies = get_enemies()
	var enemy_loc = []
	for enemy in enemies:
		var ene = enemy.get_parent()
		enemy_loc.append(tile.local_to_map(ene.position))
		
	if tile.local_to_map(position) in enemy_loc:
		return true
	return false

func check_if_end_turn():
	var teams = team_manager.get_teams() as Array[Team]
	for team: Team in teams:
		if team == turn_manager.current_team_turn():
			for char in team.members:
				if char.get_parent().get_node('TurnAction').get_action():
					return 
	turn_manager.next_turn()
	return 

func attack_mode():
	var is_player := tile.local_to_map(position) == tile.local_to_map(character.position)
	if is_player:
		character.get_node('TurnAction').set_action(false)
		check_if_end_turn()
		return true
	elif check_if_valid_target():
		deal_damage()
		character.get_node('TurnAction').set_action(false)
		check_if_end_turn()
		return true
	return false


func hide_tiles():
	for child in n.get_children():
		child.queue_free()

func gen_move_spaces():
	var data := character.get_node("MoveData") as MoveData
	var num_spaces = data.get_move_distance()
	var move_spaces = Utils.coordinates_for_movement(tile.local_to_map(character.position), num_spaces, valid_coord)
	##remove characters
	move_spaces.append(tile.local_to_map(character.position))
	return move_spaces
	
	
func move_character():
	var select_map_pos = get_tile_map_position(self)
	turn_start_pos = character.position
	var move_spaces = gen_move_spaces()
	if select_map_pos in move_spaces:
		for child in n.get_children():
			child.queue_free()
		tween_movement(character, select_map_pos)
		
func tween_movement(node, new_loc):
	var move_spaces = astar_grid.get_point_path(map_to_grid(tile.local_to_map(node.position)), map_to_grid(new_loc))
	print(move_spaces)
	for space in move_spaces:
		var m = movetile.instantiate()
		n.add_child(m)
		m.position = tile.map_to_local(grid_to_map(space))
	var tween := tween_move_space(move_spaces)
	tween.finished.connect(tween_move_finished)
	
func tween_move_space(array_of_spaces)->Tween:
	var tween = get_tree().create_tween()
	for space in array_of_spaces:
		tween.tween_property(character, "position", tile.map_to_local(grid_to_map(space)), tile_move_time)
	return tween

func tween_move_finished():
	phaseable.next_phase();

func display_attack():
	var data := character.get_node("attackData") as AttackData
	var num_spaces = data.get_attack_range()
	var attack_spaces = Utils.coordinates_for_movement(tile.local_to_map(character.position), num_spaces, valid_attack)
	for space in attack_spaces:
		var m = attacktile.instantiate()
		n.add_child(m)
		m.position = tile.map_to_local(space)
	
func  display_move():
	var move_spaces = gen_move_spaces()
	for space in move_spaces:
		var m = movetile.instantiate()
		n.add_child(m)
		m.position = tile.map_to_local(space)

func get_tile_map_position(node_2d: Node2D)->Vector2i:
	return tile.local_to_map(node_2d.position)

func valid_attack(coor):
	return tile.get_cell_source_id(3, coor) != -1 


func valid_coord(coor):
	var invalid_tiles = []
	#remove characters loc
	var teams = team_manager.get_teams() as Array[Team]
	for team: Team in teams:
		for char in team.members:
			var space = tile.local_to_map(char.get_parent().position)
			invalid_tiles.append(space)
	if coor in invalid_tiles:
		return false
	return tile.get_cell_source_id(3, coor) != -1 
	
	

