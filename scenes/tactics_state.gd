class_name TacticsState
extends State

@export var collision:Area2D
@export var selectorStart:Node2D
@export var scene = preload("res://scenes/UI/selector/selector.tscn")
var select:Selector

func _temp_exit(body):
	_exit_state()

func _enter_state() -> void:
	collision.body_entered.connect(_temp_exit)
	select = scene.instantiate() as Selector
	add_child(select)
	select.global_position = selectorStart.global_position
	state_started.emit()
	

func _exit_state() -> void:
	collision.body_entered.disconnect(_temp_exit)
	select.queue_free()
	state_finished.emit()
	
