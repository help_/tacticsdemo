extends Node
class_name AnimationSystem

@export var animated: AnimatedSprite2D
@export var move_system: MoveSystem

const direction_to_animation_name := {
	ContinuousArrowKeys.ui_directions[0]: 'right',
	ContinuousArrowKeys.ui_directions[1]: 'up',
	ContinuousArrowKeys.ui_directions[2]: 'right',
	ContinuousArrowKeys.ui_directions[3]: 'down',
}

func _ready() -> void:
	ContinuousArrowKeys.sig_direction_changed.connect(_direction_changed)
	# move_system.sig_move_started.connect(func(): _walk_animation(ContinuousArrowKeys.direction))
	move_system.sig_move_ended.connect(func(): _idle_animation(move_system.get_last_move_direction()))

func _direction_changed(direction: Vector2i):
	if direction == Vector2i.ZERO:
		return
	_walk_animation(direction)

func _idle_animation(direction: Vector2i):
	animated.flip_h = true if direction == Vector2i.LEFT else false
	animated.play('idle_' + direction_to_animation_name[direction])

func _walk_animation(direction: Vector2i):
	animated.flip_h = true if direction == Vector2i.LEFT else false
	animated.play('walk_' + direction_to_animation_name[direction])
