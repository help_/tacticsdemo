extends Node2D

var direction := Vector2i.ZERO
var is_moving := false

@export var tile_move_time = 0.5
@onready var tile_move_time_diagnol = tile_move_time * sqrt(2)

@export var buffer_movement_frame_count := 1
var buffer_movement_frames := 0
# @onready var main_grid: Grid = %MainGrid
# @onready var level_manager: LevelManager = %LevelManager
@onready var tile_map: TileMap = %TileMap

var ui_actions = [
	"ui_left",
	"ui_up",
	"ui_right",
	"ui_down"
]
var ui_directions = [
	Vector2i.LEFT,
	Vector2i.UP,
	Vector2i.RIGHT,
	Vector2i.DOWN
]

signal moved(position: Vector2i)

func _process(_delta):
	_process_movement()
	
func _process_movement():
	
	var direction_instant = Vector2i.ZERO
	for i in range(ui_actions.size()):
		if Input.is_action_just_pressed(ui_actions[i]):
			direction_instant = ui_directions[i]
			break
	if direction_instant != Vector2i.ZERO:
		direction = direction_instant
	else:
		var direction_continuous = Vector2i.ZERO
		for i in range(ui_actions.size()):
			if direction == ui_directions[i] and Input.is_action_pressed(ui_actions[i]):
				direction_continuous = ui_directions[i]
				break
		if direction_continuous != Vector2i.ZERO:
			direction = direction_continuous
		else:
			var direction_remaining = Vector2i.ZERO
			for i in range(ui_actions.size()):
				if Input.is_action_pressed(ui_actions[i]):
					direction_remaining = ui_directions[i]
					break
			if direction_remaining != Vector2i.ZERO:
				direction = direction_remaining
			else:
				direction = Vector2i.ZERO

	if direction != Vector2i.ZERO:
		if not is_moving:
			# var grid_position := main_grid.world_to_grid(self.transform.origin)
			# var does_entity_exist := level_manager.entity_exists_at(grid_position + direction)
			# if does_entity_exist:
				# return
			is_moving = true
#			if animation != "run":
#				animation = "run"
#				frame = 11
#			var is_diagnol_move = direction.x != 0 and direction.y != 0
#
#			if direction == LEFT or direction == DOWN_LEFT or direction == UP_LEFT:
#				flip_h = true
#			if direction == RIGHT or direction == DOWN_RIGHT or direction == UP_RIGHT:
#				flip_h = false

			# var world_pos := tile_map.to_global(tile_map.map_to_local(direction))
			var world_pos := direction * 16

			var origin = transform.origin
			var stop_position = Vector2(origin.x + world_pos.x, origin.y + world_pos.y)
			var tween = get_tree().create_tween()
			var move_speed = tile_move_time
#			if is_diagnol_move:
#				move_speed = tile_move_time_diagnol
			tween.tween_property(self, "position", stop_position, move_speed)
			tween.finished.connect(_movement_ended)
			# tween.tween_callback(_movement_ended)
#	elif not is_moving:
#		animation = "idle"


func _movement_ended():
	# var grid_position := main_grid.world_to_grid(Vector2(self.position.x, self.position.y))
	# moved.emit(grid_position)
	is_moving = false
