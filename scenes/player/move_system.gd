extends Node
class_name MoveSystem

@export var moved: Node2D
@export var tile_move_time = 0.1

@export var tilemap: TileMap

@onready var tile_move_time_diagnol = tile_move_time * sqrt(2)

var is_moving := false
var _sig_moved_trig := true

var _last_move_direction: Vector2i

signal sig_move_started()
signal sig_move_ended()

func _process(_delta):
	if ContinuousArrowKeys.direction != Vector2i.ZERO:
		var world_pos := ContinuousArrowKeys.direction * 16 #HELP
		var origin = moved.transform.origin
		var stop_position = Vector2(origin.x + world_pos.x, origin.y + world_pos.y)
		var tile = tilemap.local_to_map(stop_position)
		var cells = tilemap.get_used_cells(3)
		if(tile in cells ):
			if not is_moving:
				is_moving = true
				var tween = get_tree().create_tween()
				tween.tween_property(moved, "position", stop_position, tile_move_time)
				tween.finished.connect(_movement_ended)
			
			_last_move_direction = ContinuousArrowKeys.direction
			_sig_moved_trig = false
			sig_move_started.emit()

	elif not is_moving:
		if not _sig_moved_trig:
			_sig_moved_trig = true
			sig_move_ended.emit()

func _movement_ended():
	is_moving = false

func get_last_move_direction()->Vector2i:
	return _last_move_direction
