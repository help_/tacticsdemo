class_name HealthData
extends Node

@export var starting_health = 10

signal sig_died()

var current_health

func _ready():
	current_health = starting_health
	

func get_current_health():
	return current_health

func damage_health(damage):
	current_health = current_health-damage
	if current_health < 0:
		get_parent().get_node("AnimatedSprite2D").play('death_right')
