class_name AttackData
extends Node

@export var attack_damage = 3 
@export var attack_range := 1

func get_attack_damage():
	return attack_damage

func get_attack_range()->int:
	return attack_range
