extends Node2D

@export var moved: Node2D
@export var target: Node
@export var tilemap: TileMap
@export var tile_move_time = 0.1

var leader_current_pos 
var leader_previous_pos
# Called when the node enters the scene tree for the first time.

func _ready():
	leader_current_pos = tilemap.local_to_map(target.transform.origin)
	


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if (tilemap.local_to_map(target.transform.origin) != leader_current_pos):
		leader_previous_pos = leader_current_pos
		leader_current_pos = tilemap.local_to_map(target.transform.origin)
		var tween = get_tree().create_tween()
		tween.tween_property(moved, "position", tilemap.map_to_local(leader_previous_pos), tile_move_time)

