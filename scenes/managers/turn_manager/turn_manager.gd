extends Node
class_name TurnManager

@export var turn_order: Array[Team] = []
@export var turn_index: int = -1

signal sig_turn_changed(from: Team, to: Team)

@export var team_manager: TeamManager

func _enter_tree() -> void:
	pass
	# (%PhaseManager as PhaseManager).sig_done_phase_began.connect(_on_member_turn_ended, CONNECT_DEFERRED)

func _ready() -> void:
	# get_node("/root").ready.connect(_all_ready)
	team_manager.sig_roster_updated.connect(_team_changed)
	call_deferred('next_turn')

func _all_ready():
	turn_order = team_manager.get_teams()

# func _on_member_turn_ended(phaseable: Phaseable):
# 	var node = phaseable.owner
# 	if not node.has_node("Teamable"):
# 		return
# 	var teamable := node.get_node("Teamable") as Teamable
# 	if not turn_order.has(teamable.team):
# 		return
# 	for member in teamable.team.members:
# 		node = member.owner
# 		if not node.has_node("Phaseable"):
# 			continue
# 		phaseable = node.get_node("Phaseable") as Phaseable
# 		if not phaseable.is_done():
# 			return
# 	call_deferred("next_turn")
	
func next_turn():
	#are there any players?
	if not team_manager.is_any_team():
		return
		
	var previous_team = current_team_turn()
	turn_index += 1
	turn_index %= len(turn_order)
	var current_team = current_team_turn()
	
	sig_turn_changed.emit(previous_team, current_team)

func current_team_turn() -> Team:
	if turn_index == -1:
		return null
	return turn_order[turn_index]

func _team_changed(team: Team):
	#all teams accounted for?
	if len(team_manager.get_teams()) == len(turn_order):
		return
		
	if team_manager.has_team(team):
		#add team
		turn_order.push_back(team)
	else:
		var is_players_turn = current_team_turn() == team
		
		#remove team
		var index = turn_order.find(team)
		if index < turn_index:
			turn_index -= 1
		turn_order.erase(team)
		
		#turn ends immediately for that player
		if is_players_turn:
			next_turn()

func sort_player_first():
	pass
