extends Node
class_name TeamManager

signal sig_roster_updated(team: Team)

# var _teams := {} #type team dictionary
var _teams: Array[Team]

@export var player_team: Team

func _ready():
	# get_node("/root").ready.connect(_all_ready)
	_update_teams()

	# player_team.number = Team.PLAYER_TEAM_NUMBER

func _all_ready():
	_assign_missing_team_numbers()

#public
func get_team(team_number: int) -> Team:
	if has_team_number(team_number):
		return _teams.filter(func (team: Team): return team.number == team_number)[0]
	return null

func get_teams() -> Array[Team]:
	return _teams

func has_team_number(team_number: int) -> bool:
	return _teams.find(func (team: Team): return team.number == team_number) != -1

func has_team(team: Team) -> bool:
	return _teams.has(team)

func get_player_team():
	return player_team

func is_any_team() -> bool:
	return len(_teams) > 0

#private
func _update_teams():
	var updated_teams: Array[Team] = []
	for child in get_children():
		updated_teams.push_back(child as Team)
	_teams = updated_teams

func _assign_missing_team_numbers():
	for i in len(_teams):
		var team := _teams[i]
		if team.number == -1:
			team.number = i
			sig_roster_updated.emit(team)

func _new_team(team_number: int) -> Team:
	if has_team_number(team_number):
		return null
	var team := Team.new()
	team.number = team_number
	return team
	
func _add_team(team: Team):
	add_child(team)
	_update_teams()
	sig_roster_updated.emit(team)
	
func _remove_team(team: Team):
	team.get_parent().remove_child(team)
	_update_teams()
	sig_roster_updated.emit(team)
	
func _add_team_member(team: Team, teamable: Teamable):
	if not has_team(team):
		_add_team(team)
	team.add_member(teamable)
	teamable.team = team
	
func _remove_team_member(team: Team, teamable: Teamable):
	team.remove_member(teamable)
	teamable.team = null
	print(team.is_roster_empty())
	if team.is_roster_empty():
		_remove_team(team)



# func _on_cruncher_manager_sig_cruncher_added(cruncher: Cruncher):
# 	var teamable := cruncher.teamable
# 	var team := teamable.team
# 	if team == null:
# 		team = _new_team(_teams.size())
# 	_add_team_member(team, teamable)


# func _on_cruncher_manager_sig_cruncher_removed(cruncher) -> void:
# 	var team: Team = cruncher.teamable.team
# 	if team == null:
# 		return
# 	_remove_team_member(team, cruncher.teamable)
