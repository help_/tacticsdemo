extends Node
class_name Team

@export var number: int = -1
@export var members: Array[Teamable] = []

const PLAYER_TEAM_NUMBER = 1

func add_member(teamable: Teamable):
	if not members.has(teamable):
		members.push_back(teamable)

func remove_member(teamable: Teamable):
	if members.has(teamable):
		members.erase(teamable)

func is_roster_empty() -> bool:
	return len(members) == 0
