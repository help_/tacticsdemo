class_name BasicMoveSystem
extends Node

@export var tile_move_time = 0.1

var pixel_size:int = 16
var is_moving := false
var parent:Node2D
var _sig_moved_trig := true

signal sig_move_started()
signal sig_move_ended()

func _ready():
	parent = get_parent()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if ContinuousArrowKeys.direction != Vector2i.ZERO:
		var world_pos := ContinuousArrowKeys.direction * pixel_size #HELP
		var origin = parent.global_position
		var stop_position = Vector2(origin.x + world_pos.x, origin.y + world_pos.y)
		if not is_moving:
			is_moving = true
			var tween = get_tree().create_tween()
			tween.tween_property(parent, "position", stop_position, tile_move_time)
			tween.finished.connect(_movement_ended)
		
		_sig_moved_trig = false
		sig_move_started.emit()
	elif not is_moving:
		if not _sig_moved_trig:
			_sig_moved_trig = true
			sig_move_ended.emit()

func _movement_ended():
	is_moving = false
